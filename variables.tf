variable "name" {
  type        = "string"
  description = "The replication group identifier. This parameter is stored as a lowercase string."
}

variable "num_cache_nodes" {
  type        = "string"
  description = "The number of cache nodes."
  default     = "1"
}

variable "node_type" {
  type        = "string"
  description = "The compute and memory capacity of the nodes in the node group."
}

variable "subnet_ids" {
  type        = "list"
  description = "List of VPC Subnet IDs for the cache subnet group."
}

variable "vpc_id" {
  type        = "string"
  description = "VPC Id to associate with Redis ElastiCache."
}

variable "source_security_group_id" {
  type        = "list"
  description = "List of Security Groups allowed to access Elasticache."
  default     = []
}

variable "engine" {
  type        = "string"
  description = "engine type allowed values are redis or memcached"
  default     = ""
}

variable "engine_version" {
  default     = ""
  type        = "string"
  description = "The version number of the cache engine to be used for the cache clusters in this replication group."
}

variable "port" {
  type        = "string"
  description = "The port number on which each of the cache nodes will accept connections."
}

variable "maintenance_window" {
  default     = ""
  type        = "string"
  description = "Specifies the weekly time range for when maintenance on the cache cluster is performed."
}

variable "snapshot_window" {
  default     = ""
  type        = "string"
  description = "The daily time range (in UTC) during which ElastiCache will begin taking a daily snapshot of your cache cluster."
}

variable "snapshot_retention_limit" {
  default     = ""
  type        = "string"
  description = "The number of days for which ElastiCache will retain automatic cache cluster snapshots before deleting them."
}

variable "apply_immediately" {
  default     = false
  type        = "string"
  description = "Specifies whether any modifications are applied immediately, or during the next maintenance window."
}

variable "family" {
  default     = ""
  type        = "string"
  description = "The family of the ElastiCache parameter group."
}

variable "description" {
  default     = "Managed by Terraform"
  type        = "string"
  description = "The description of the all resources."
}

variable "tags" {
  default     = {}
  type        = "map"
  description = "A mapping of tags to assign to all resources."
}

variable "az_mode" {
  type        = "string"
  default     = "single-az"
  description = "Valid values for this parameter are single-az or cross-az"
}

variable "preferred_availability_zones" {
  type    = "list"
  default = []
}

variable "parameter" {
  type        = "list"
  default     = []
  description = "A list of parameters to apply."
}
